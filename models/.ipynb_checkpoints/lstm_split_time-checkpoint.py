import torch
import torch.nn as nn
from torch.autograd import Variable

class lstm(nn.Module):
    def __init__(self, input_size, output_size, hidden_size, n_layers, batch_size):
        super(lstm, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.hidden_size = hidden_size
        self.batch_size = batch_size
        self.n_layers = n_layers
        self.embed = nn.Linear(input_size, hidden_size)
        self.lstm = nn.ModuleList([nn.LSTMCell(hidden_size, hidden_size) for i in range(self.n_layers)])
        self.output = nn.Sequential(
                nn.Linear(hidden_size, output_size),
                #nn.BatchNorm1d(output_size),
                nn.Tanh())
        self.hidden = self.init_hidden()

    def init_hidden(self):
        hidden = []
        for i in range(self.n_layers):
            hidden.append((Variable(torch.zeros(self.batch_size, self.hidden_size).cuda()),
                           Variable(torch.zeros(self.batch_size, self.hidden_size).cuda())))
        return hidden

    def forward(self, input):

        embedded = self.embed(input.view(-1, self.input_size))
        h_in = embedded
        for i in range(self.n_layers):
            self.hidden[i] = self.lstm[i](h_in, self.hidden[i])
            h_in = self.hidden[i][0]

        return self.output(h_in)

class gaussian_lstm(nn.Module):
    def __init__(self, input_size, output_size, hidden_size, n_layers, batch_size):
        super(gaussian_lstm, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.batch_size = batch_size
        self.input_size = int(input_size/2)
        self.embed = nn.Linear(self.input_size, hidden_size)
        self.lstm = nn.ModuleList([nn.LSTMCell(hidden_size, hidden_size) for i in range(self.n_layers)])
        
        self.mu_t_net = nn.Linear(hidden_size, output_size)
        self.logvar_t_net = nn.Linear(hidden_size, output_size)
        self.hidden = self.init_hidden()
        
        self.mu_net1 = nn.Linear(self.input_size, hidden_size)
        self.mu_net2 = nn.Linear(hidden_size, output_size)
        
        self.logvar_net1 = nn.Linear(self.input_size, hidden_size)
        self.logvar_net2 = nn.Linear(hidden_size, output_size)

    def init_hidden(self):
        hidden = []
        for i in range(self.n_layers):
            hidden.append((Variable(torch.zeros(self.batch_size, self.hidden_size).cuda()),
                           Variable(torch.zeros(self.batch_size, self.hidden_size).cuda())))
        return hidden

    def reparameterize(self, mu, logvar):
        logvar = logvar.mul(0.5).exp_()
        eps = Variable(logvar.data.new(logvar.size()).normal_())
        return eps.mul(logvar).add_(mu)
    
    def mu_net(self, input):
        out = self.mu_net1(input)
        out = self.mu_net2(out)
        return out

    def logvar_net(self, input):
        out = self.logvar_net1(input)
        out = self.logvar_net2(out)
        return out
    
    def forward(self, input):

        input_t, input = torch.split(input, self.input_size, dim=1)
        embedded = self.embed(input_t.view(-1, self.input_size))
        h_in = embedded
        for i in range(self.n_layers):
            self.hidden[i] = self.lstm[i](h_in, self.hidden[i])
            h_in = self.hidden[i][0]
        mu = self.mu_net(input)
        logvar = self.logvar_net(input)
        z = self.reparameterize(mu, logvar)
        
        mu_t = self.mu_t_net(h_in)
        logvar_t = self.logvar_t_net(h_in)
        z_t = self.reparameterize(mu_t, logvar_t)
        return z_t, mu_t, logvar_t, z, mu, logvar
            
